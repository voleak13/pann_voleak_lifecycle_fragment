package com.hrd.lifecyclefragmentjava;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    private static final String COMMON_TAG = "CombinedLifeCycle";
    private static final String ACTIVITY_NAME = MainActivity.class.getSimpleName();
    private static final String TAG = COMMON_TAG;
    private Button buttonAddFragment;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        buttonAddFragment=findViewById(R.id.buttonAddFragment);
        buttonAddFragment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addFragment();
            }
        });
        Log.i(TAG,ACTIVITY_NAME+" onCreate");
    }
//    @Override
//    protected void onStart() {
//        super.onStart();
//        Log.i(TAG,ACTIVITY_NAME+" onStart");
//    }
//    @Override
//    protected void onResume() {
//        super.onResume();
//        Log.i(TAG,ACTIVITY_NAME+" onResume");
//    }
//    @Override
//    protected void onPause() {
//        super.onPause();
//        Log.i(TAG,ACTIVITY_NAME+" onPause");
//    }
//    @Override
//    protected void onStop() {
//        super.onStop();
//        Log.i(TAG,ACTIVITY_NAME+" onStop");
//    }
//    @Override
//    protected void onDestroy() {
//        super.onDestroy();
//        Log.i(TAG,ACTIVITY_NAME+" onDestroy");
//    }
    public void  addFragment()
    {
        Fragment fragment=new SimpleFragment();
        FragmentManager fragmentManager=getSupportFragmentManager();
        FragmentTransaction fragmentTransaction=fragmentManager.beginTransaction();
        fragmentTransaction.add(R.id.fragmentContainer,fragment,"demoFragment");
        fragmentTransaction.addToBackStack("fragmentStack1");
        fragmentTransaction.commit();
    }
}