package com.hrd.lifecyclefragmentjava;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


    public class SimpleFragment extends Fragment {
        private static final String COMMON_TAG = "CombinedLifeCycle";
        private static final String FRAGMENT_NAME = SampleFragment.class.getSimpleName();
        private static final String TAG = COMMON_TAG;
        @Override
        public void onAttach(@NonNull Context context) {
            super.onAttach(context);
            Log.i(TAG,FRAGMENT_NAME+" onAttach");
        }
        @Override
        public void onCreate(@Nullable Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            Log.i(TAG,FRAGMENT_NAME+" onCreate");
        }
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Log.i(TAG,FRAGMENT_NAME+" onCreateView");
        return super.onCreateView(inflater, container, savedInstanceState);
    }
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        Log.i(TAG,FRAGMENT_NAME+" onActivityCreated");
        super.onActivityCreated(savedInstanceState);
    }
    @Override
    public void onStart() {
        super.onStart();
        Log.i(TAG,FRAGMENT_NAME+" onStart");
    }
    @Override
    public void onResume() {
        super.onResume();
        Log.i(TAG,FRAGMENT_NAME+" onResume");
    }
    @Override
    public void onPause() {
        super.onPause();
        Log.i(TAG,FRAGMENT_NAME+" onPause");
    }
    @Override
    public void onStop() {
        super.onStop();
        Log.i(TAG,FRAGMENT_NAME+" onStop");
    }
    @Override
    public void onDestroyView() {
        super.onDestroyView();
        Log.i(TAG,FRAGMENT_NAME+" onDestroyView");
    }
    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.i(TAG,FRAGMENT_NAME+" onDestroy");
    }
    @Override
    public void onDetach() {
        super.onDetach();
        Log.i(TAG,FRAGMENT_NAME+" onDetach");
    }
}
